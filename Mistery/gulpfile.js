'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

gulp.task('sass', function () {
    return gulp.src('app/scss/app.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});
gulp.task('scss:watch', function () {
    gulp.watch('app/scss/app.scss', ['sass']);
});
gulp.task('copy', function () {
    gulp.src(['app/bower_components/**/*.js', 'app/components/**/*.js'])
        .pipe(gulp.dest('app/script'));
    gulp.src(['app/bower_components/**/*.css', 'app/components/**/*.css'])
        .pipe(gulp.dest('app/css/distribution'))
});
gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: 'app'
        }
    })
});
gulp.task('watch', ['browserSync', 'sass', 'copy'], function () {
    gulp.watch('app/scss/app.scss', ['sass']);
    // другие ресурсы
});