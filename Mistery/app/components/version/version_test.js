'use strict';

describe('MisTeryApp.version module', function() {
  beforeEach(module('MisTeryApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
