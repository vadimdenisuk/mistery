'use strict';

angular.module('MisTeryApp.version', [
  'MisTeryApp.version.interpolate-filter',
  'MisTeryApp.version.version-directive'
])

.value('version', '0.1');
