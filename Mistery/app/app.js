'use strict';

// Declare app level module which depends on views, and components
angular.module('MisTeryApp', [
    'ngRoute',
    'MisTeryApp.home',
    'MisTeryApp.version'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/home'});
}]);
