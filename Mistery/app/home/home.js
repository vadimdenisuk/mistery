'use strict';

angular.module('MisTeryApp.home', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'home/home.html',
            controller: 'HomeController'
        });
    }])

    .controller('HomeController', ['$scope', '$http', function ($scope, $http) {
        $scope.scrollLeft = function () {
            var scrollPosition = $(".photoSlider").scrollLeft();
            $('.photoSlider').animate({scrollLeft: scrollPosition - 450}, 350);
        };
        $scope.scrollRight = function () {
            var scrollPosition = $(".photoSlider").scrollLeft();
            $('.photoSlider').animate({scrollLeft: scrollPosition + 450}, 350);
        };
        var imagesSocial = [];
        for (var i = 1; i < 10; i++)
        {
            imagesSocial.push({link: "/#", img: '/image/SocialIcon/' + i + '.png'});
        }
        $scope.socialNetwork = imagesSocial;
        // $http.get('http://beta.json-generator.com/api/json/get/VymPZ9U4z')
        //     .success(function (data, staus) {
        //         $scope.socialNetwork = data;
        //     })
        //     .error(function (data, status) {
        //         var images = [];
        //         for (var i = 1; i < 10; i++)
        //         {
        //             images.push({link: "/#", img: '/image/SocialIcon/' + i + '.png'});
        //         }
        //         $scope.socialNetwork = images;
        //     });
        var imagesBigPickture = [];
        for (var i = 1; i < 8; i++)
        {
            imagesBigPickture.push('/image/photo/' + i + '.jpg');
        }
        $scope.imageArray = imagesBigPickture;
        // $http.get('http://beta.json-generator.com/api/json/get/4JNb79UEM')
        //     .success(function (data, staus) {
        //         $scope.imageArray = data;
        //     })
        //     .error(function (data, status) {
        //         var images = [];
        //         for (var i = 1; i < 8; i++)
        //         {
        //             images.push('/image/photo/' + i + '.jpg');
        //         }
        //         $scope.imageArray = images;
        //         console.log(data, status);
        //     });
        $scope.openPhoto = function (index) {
            $('#myModal').modal('show');
            $scope.imageIndex = index;
            $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
        };
        $scope.nextPhoto = function () {
            if ($scope.imageArray.length > $scope.imageIndex + 1) {
                $scope.imageIndex = $scope.imageIndex + 1;
                $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
            } else {
                $scope.imageIndex = 0;
                $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
            }
        };
        $scope.changeIndex = function ($event) {
            if ($event.keyCode == 39)
                if ($scope.imageArray.length > $scope.imageIndex + 1) {
                    $scope.imageIndex = $scope.imageIndex + 1;
                    $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
                } else {
                    $scope.imageIndex = 0;
                    $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
                }
            else if ($event.keyCode == 37)
                if ($scope.imageArray.length > $scope.imageIndex + 1) {
                    if ($scope.imageIndex == 0) {
                        $scope.imageIndex = $scope.imageArray.length - 1;
                    }
                    else {
                        $scope.imageIndex = $scope.imageIndex - 1;
                    }

                    $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
                } else {
                    $scope.imageIndex = 0;
                    $scope.imageUrl = $scope.imageArray[$scope.imageIndex];
                }
        }

    }]);